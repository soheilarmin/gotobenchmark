import QtQuick 2.15
import QtQuick.Window 2.15
import Qt.labs.qmlmodels 1.0
import QtCharts 2.1
import GotoBenchmark 1.0

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Breaking Nested Loops - Performance comparison : goto vs flag")


    GotoBenchmark{
        id: gotoBenchmark
        running: true

        property real maxY: 0

        property TableModel gotoResultsModel: TableModel{
            id: gotoResultsModel
            TableModelColumn { display: "x" }
            TableModelColumn { display: "y" }
            rows: [{x: 0,y: 0}]
        }

        property TableModel flagResultsModel: TableModel{
            id: flagResultsModel
            TableModelColumn { display: "x" }
            TableModelColumn { display: "y" }
            rows: [{x: 0,y: 0}]
        }

        onFlagElapsed: {

            let ms = elapsed/1000000.0;
            flagResultsModel.appendRow({
                                           x: gotoResultsModel.rowCount ,
                                           y: ms
                                       })
            gotoBenchmark.maxY = Math.max(gotoBenchmark.maxY,ms)


        }
        onGotoElapsed: {
            let ms = elapsed/1000000.0;
            gotoResultsModel.appendRow({
                                           x: flagResultsModel.rowCount ,
                                           y: ms
                                       })
             gotoBenchmark.maxY = Math.max(gotoBenchmark.maxY,ms)
        }
    }


    ChartView {
        id: chartView
        anchors.fill: parent
        animationOptions: ChartView.NoAnimation
        theme: ChartView.ChartThemeDark

        ValueAxis {
            id: axisY
            min: 0
            max: gotoBenchmark.maxY * 1.1
        }

        ValueAxis {
            id: axisX
            min: Math.max(0,flagResultsModel.rowCount-128)
            max: flagResultsModel.rowCount - 1
        }

        LineSeries  {
            id: lineSeries1
            name: "\"goto\" impl. duration"
            axisX: axisX
            axisY: axisY
            color: 'green'
            width: 2
            useOpenGL: true
            VXYModelMapper{
                model: gotoResultsModel
                xColumn: 0
                yColumn: 1
            }
        }

        LineSeries  {
            id: lineSeries2
            name: "\"flag\" impl. duration"
            axisX: axisX
            axisY: axisY
            color: 'red'
            width: 2
            useOpenGL: true
            VXYModelMapper{
                model: flagResultsModel
                xColumn: 0
                yColumn: 1
            }
        }
    }
}
