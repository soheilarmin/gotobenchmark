#include "GotoBenchmark.h"
#include <QElapsedTimer>
#include <QtMath>
#include <QDebug>

GotoBenchmark::outcome GotoBenchmark::testGoto(quint32 loops)
{
    QElapsedTimer t; t.restart();
    double result = 0;
    quint64 callCounter = 0;

    for(quint32 i=0 ; i<loops ; ++i){
        for(quint32 j=0 ; j<loops ; ++j){
            for(quint32 k=0 ; k<loops ; ++k){
                if(k==loops - 9)
                    goto broken;
                result += (i + j + k);
                callCounter++;
            }
        }
    }

    broken:
    return {t.nsecsElapsed(),callCounter,result};
}

GotoBenchmark::outcome GotoBenchmark::testFlag(quint32 loops)
{
    QElapsedTimer t; t.restart();

    double result = 0;
    quint64 callCounter = 0;

    bool broken=false;

    for(quint32 i=0 ; i<loops && !broken ; ++i){
        for(quint32 j=0 ; j<loops && !broken ; ++j){
            for(quint32 k=0 ; k<loops && !broken ; ++k){
                if(k==loops-10)
                    broken=true;
                result += (i + j + k);
                callCounter++;
            }
        }
    }

    return {t.nsecsElapsed(),callCounter,result};
}

void GotoBenchmark::startGotoTest()
{

    if (gotoResultWatcher==nullptr){
        gotoResultWatcher = new QFutureWatcher<outcome>(this);

        connect(gotoResultWatcher, &QFutureWatcher<outcome>::finished,this, [&](){
            emit gotoElapsed(gotoResultWatcher->result().elapsed);
            if(m_running){
                startGotoTest();
            }
        });
    }


    auto futureResult = QtConcurrent::run(QThreadPool::globalInstance(),[=]() -> outcome {
        return this->testGoto();
    });
    gotoResultWatcher->setFuture(futureResult);
}

void GotoBenchmark::startFlagTest()
{

    if (flagResultWatcher==nullptr){
        flagResultWatcher = new QFutureWatcher<outcome>(this);

        connect(flagResultWatcher, &QFutureWatcher<outcome>::finished,this, [&](){
            emit flagElapsed(flagResultWatcher->result().elapsed);
            if(m_running)
                startFlagTest();
        });
    }

    auto futureResult = QtConcurrent::run(QThreadPool::globalInstance(),[=]() -> outcome {
        return this->testFlag();
    });
    flagResultWatcher->setFuture(futureResult);
}


bool GotoBenchmark::running() const
{
    return m_running;
}

void GotoBenchmark::setRunning(bool newRunning)
{
    if (m_running == newRunning)
        return;
    m_running = newRunning;
    if(m_running){
        startFlagTest();
        startGotoTest();
    }

    emit runningChanged();
}
