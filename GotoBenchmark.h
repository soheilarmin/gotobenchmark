#ifndef GOTOBENCHMARK_H
#define GOTOBENCHMARK_H

#include <QtConcurrent/QtConcurrent>
#include <QCoreApplication>
#include <QQmlEngine>
#include <QObject>
#include <tuple>


class GotoBenchmark : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY runningChanged)

public:
    explicit GotoBenchmark(QObject *parent = nullptr):
        QObject(parent),
        m_running(false)
    {};

    struct outcome{
        qint64 elapsed;
        quint64 callCount;
        double result;
    };

    Q_INVOKABLE outcome testGoto(quint32 loops = 20000000);
    Q_INVOKABLE outcome testFlag(quint32 loops = 20000000);

    bool running() const;
    void setRunning(bool newRunning);

private:

   bool m_running;
   QFutureWatcher<outcome> *gotoResultWatcher{nullptr};
   QFutureWatcher<outcome> *flagResultWatcher{nullptr};

   void startGotoTest();
   void startFlagTest();

signals:

   void runningChanged();

   void gotoElapsed(quint64 elapsed);
   void flagElapsed(quint64 elapsed);
};


static void registerGotoBenchmark()
{
    qmlRegisterType<GotoBenchmark>("GotoBenchmark", 1, 0, "GotoBenchmark");
}

Q_COREAPP_STARTUP_FUNCTION(registerGotoBenchmark)

#endif // GOTOBENCHMARK_H
